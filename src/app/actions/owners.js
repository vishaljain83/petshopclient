import axios from 'axios';

import { ROOT_URL } from './index';

export const FETCH_OWNERS = 'FETCH_OWNERS';

export function fetchOwners(){
    const url = `${ROOT_URL}/owners`;
    const request = axios.get(url);
    
    return {
        type: FETCH_OWNERS,
        payload: request
    }
}