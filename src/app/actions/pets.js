import axios from 'axios';

import { ROOT_URL } from './index';

export const FETCH_PETS = 'FETCH_PETS';
export const ADD_PET = 'ADD_PET';

export function fetchPets(){
    const url = `${ROOT_URL}/pets`;
    const request = axios.get(url);
    
    return {
        type: FETCH_PETS,
        payload: request
    }
}

export function addPet(name, birthday, owner_id){
    const url = `${ROOT_URL}/pet`;

    let data = JSON.stringify({
        name: name,
        birthday: birthday,
        owner_id: owner_id
    })

    axios.post(url, { data, 
    }).then((response) => {
        console.log('saved successfully')
    });
}