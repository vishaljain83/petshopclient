import React, { Component } from 'react';

import ControlBar from '../containers/control_bar';


export default class App extends Component {
  render() {
    return (
      <div>
        <ControlBar />
      </div>
    );
  }
}