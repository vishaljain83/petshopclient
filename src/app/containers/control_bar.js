import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchOwners } from '../actions/owners';
import { fetchPets } from '../actions/pets';

import OwnersList from '../containers/owners_list';
import PetsList from '../containers/pets_list';
import PetForm from '../containers/pet_form';

class ControlBar extends Component {

    constructor(props)
    {
        super(props)

        this.state = { showOwners: false,
                       showPets: false,
                       addPet:false
                     };

        this.getOwners = this.getOwners.bind(this);
        this.getPets = this.getPets.bind(this);
        this.addPet = this.addPet.bind(this);
    }

    getOwners(event){
        this.props.fetchOwners();

        this.setState({
            showOwners: true,
            showPets: false,
            addPet: false
        });
    }

    getPets(event){
        this.props.fetchPets();

        this.setState({
            showOwners: false,
            showPets: true,
            addPet: false
        });
    }

    addPet(event){
        this.setState({
            showOwners: false,
            showPets: false,
            addPet: true
        });
    }

    render() {
        return (
            <div>
                <div>
                    <button type="submit" className="btn btn-secondary" onClick={this.getOwners}>Get Owners</button>
                    { this.state.showOwners ? <OwnersList /> : null }
                </div>
                <div>
                    <button type="submit" className="btn btn-secondary" onClick={this.getPets}>Get Pets</button>
                    { this.state.showPets ? <PetsList /> : null }
                </div>
                <div />
                <div>
                    <button type="submit" className="btn btn-secondary" onClick={this.addPet}>Add a Pet</button>
                    { this.state.addPet ? <PetForm /> : null }
                </div>
            </div>
        )
    }
}

function mapDispatchToProps(dispatch){
    return ( bindActionCreators ({ fetchOwners, fetchPets }, dispatch) );
}

export default connect(null, mapDispatchToProps)(ControlBar);