import React, { Component } from 'react';
import { connect } from 'react-redux';

class OwnersList extends Component {

    renderOwner(owner){
        const owner_id = owner.owner_id;
        const name = owner.first_name + ' ' + owner.last_name;
        const city = owner.city;
        const pet_id = owner.pet_id;

        return(
            <tr key={ owner_id }>
                <td>{name}</td>
                <td>{city}</td>
                <td>{pet_id}</td>
            </tr>
        );
    }

    render() {
        return(
            <table className="table table-hover">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>City</th>
                        <th>Pet ID</th>
                    </tr>
                </thead>
                <tbody>
                    { 
                        this.props.owners[0] !== undefined ? this.props.owners[0].map(this.renderOwner) : null
                    }
                </tbody>
            </table>
        );
    }
}

function mapStateToProps({ owners }) {
    return { owners };
}

export default connect(mapStateToProps)(OwnersList);