import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { addPet } from '../actions/pets';

class PetForm extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            birthday: '',
            owner_id: ''
        };

        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleBirthDayChange = this.handleBirthDayChange.bind(this);
        this.handleOwnerIDChange = this.handleOwnerIDChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
  
    handleNameChange(event) {
        this.setState({name: event.target.value});
    }
  
    handleBirthDayChange(event) {
        this.setState({birthday: event.target.value});
    }

    handleOwnerIDChange(event) {
        this.setState({owner_id: event.target.value});
    }

    handleSubmit(event) {
        event.preventDefault();

        this.props.addPet(
            this.state.name, 
            this.state.birthday, 
            this.state.owner_id
        );

        this.setState({ 
            name: '',
            birthday: '',
            owner_id: ''
        });
    }
  
    render() {
      return (
        <form onSubmit={this.handleSubmit}>
          <div>
            Name:
            <input type="text" value={this.state.name} onChange={this.handleNameChange} />
          </div>
          <div>
            Birthday:
            <input type="text" value={this.state.birthday} onChange={this.handleBirthDayChange} />
          </div>
          <div>
            Owner ID:
            <input type="text" value={this.state.owner_id} onChange={this.handleOwnerIDChange} />
          </div>
          <input type="submit" value="Submit" />
        </form>
      );
    }
}

function mapDispatchToProps(dispatch){
    return ( bindActionCreators ({ addPet }, dispatch) );
}

export default connect(null, mapDispatchToProps)(PetForm);