import React, { Component } from 'react';
import { connect } from 'react-redux';

class PetsList extends Component {

    renderPet(pet){
        const name = pet.name;
        const birthday = pet.birthday.slice(0, pet.birthday.indexOf('T'));
        const owner_id = pet.owner_id;

        return(
            <tr key={ name }>
                <td>{name}</td>
                <td>{birthday}</td>
                <td>{owner_id}</td>
            </tr>
        );
    }

    render() {
        return(
            <table className="table table-hover">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Birthday</th>
                        <th>Owner ID</th>
                    </tr>
                </thead>
                <tbody>
                    { 
                        this.props.pets[0] !== undefined ? this.props.pets[0].map(this.renderPet) : null
                    }
                </tbody>
            </table>
        );
    }
}

function mapStateToProps({ pets }) {
    return { pets };
}

export default connect(mapStateToProps)(PetsList);