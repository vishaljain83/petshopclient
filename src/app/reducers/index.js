import { combineReducers } from 'redux';

import OwnersReducer from './reducer_owners';
import PetsReducer from './reducer_pets';

const rootReducer = combineReducers({
    owners: OwnersReducer,
    pets: PetsReducer
});

export default rootReducer;