import { FETCH_OWNERS } from '../actions/owners';

export default function (state = [], action){

    switch(action.type){
        case FETCH_OWNERS:
            return [ action.payload.data.data, ...state ];
    }

    return state;
}