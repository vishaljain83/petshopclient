import { FETCH_PETS } from '../actions/pets';

export default function (state = [], action){

    switch(action.type){
        case FETCH_PETS:
            return [ action.payload.data.data, ...state ];
    }

    return state;
}