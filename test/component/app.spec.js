import React from 'react';
import App from '../../src/app/components/app';

describe('App' , () => {
    const wrapper = shallow(<App />);

    it('renders something', () => {
        expect(wrapper).to.exist;
    });
});